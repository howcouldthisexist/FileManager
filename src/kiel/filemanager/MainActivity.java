
package kiel.filemanager;

import java.io.File;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity 

	{
	
	private static String TAG = MainActivity.class.getSimpleName();
	
	ExpandableListView lv;
	LocalAdapter la = new LocalAdapter();
	File pwd;
	BroadcastReceiver br;
	

	protected void onCreate(Bundle savedInstanceState) 
	
		{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		updatePwd(Environment.getExternalStorageDirectory());
		
		lv = super.findViewById(R.id.list);
		lv.setAdapter(la);
		lv.setOnChildClickListener(new LocalClickListener());
		lv.setOnItemLongClickListener(new LocalLongClickListener());
		lv.setOnGroupClickListener(new LocalGroupClickListener());
		}
	
	protected void onStart()
	
		{
		super.onStart();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
		br = new LocalBroadcastReceiver();
		super.registerReceiver(br, filter);
		}

	public boolean onCreateOptionsMenu(Menu menu) 
	
		{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
		}

	public boolean onOptionsItemSelected(MenuItem item) 
	
		{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
		}

	protected void onStop()
	
		{
		super.onStop();
		super.unregisterReceiver(br);
		}
	
	
	//
	
	public void onBackPressed()
	
		{
		File parent = pwd.getParentFile();
		
		if (parent != null)
			updatePwd(parent);
		
		}
	
	
	//
	
	private void updatePwd(File dir)
	
		{
		
		if (dir.isDirectory())
			
			{
			this.pwd = dir;
			super.getActionBar().setTitle(dir.getName());
			la.notifyDataSetChanged();
			}
		
		else
			Log.d(TAG, "Cannot Open " + dir.getName() + " is a File");
		
		}
	
	
	
	// Local Event Handlers
	
	private class LocalClickListener implements ExpandableListView.OnChildClickListener
	
		{
		
		public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) 
		
			{
			
			
			if (pwd.listFiles()[groupPosition].listFiles()[childPosition].isFile())
				
				{
				File file = pwd.listFiles()[groupPosition].listFiles()[childPosition];
				MimeTypeMap myMime = MimeTypeMap.getSingleton();
				Intent newIntent = new Intent(Intent.ACTION_VIEW);
				String x = file.getName();
				String[] y = x.split("\\.");
				String mimeType = myMime.getMimeTypeFromExtension(y[1]);
				
				newIntent.setDataAndType(Uri.fromFile(file), mimeType);
				newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				try {
				    startActivity(newIntent);
				} catch (ActivityNotFoundException e) {
				    Toast.makeText(getApplicationContext(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
				
					}
				}
				
				else
					
				{
				updatePwd(pwd.listFiles()[groupPosition].listFiles()[childPosition]);	
				}
			
			return true;
			}
		
		}
	
	private class LocalGroupClickListener implements ExpandableListView.OnGroupClickListener
	
		{

		public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) 
		
			{
			
			if (pwd.listFiles()[groupPosition].isFile())
				
				{
				File file = pwd.listFiles()[groupPosition];
				MimeTypeMap myMime = MimeTypeMap.getSingleton();
				Intent newIntent = new Intent(Intent.ACTION_VIEW);
				String x = file.getName();
				String[] y = x.split("\\.");
				String mimeType = myMime.getMimeTypeFromExtension(y[1]);
				
				newIntent.setDataAndType(Uri.fromFile(file), mimeType);
				newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				try {
				    startActivity(newIntent);
				} catch (ActivityNotFoundException e) {
				    Toast.makeText(getApplicationContext(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
				
					}
				}
			
			else if (parent.isGroupExpanded(groupPosition) == false)
				parent.expandGroup(groupPosition);
			
			else
				parent.collapseGroup(groupPosition);
			
			return true;
			}
		
		}
	
	private class LocalLongClickListener implements OnItemLongClickListener
	
		{

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			Log.d(TAG, "LOng click");
			return true;
		}
		
		
		
		}
	

	
	//
	
	private class LocalAdapter extends BaseExpandableListAdapter
	
		{
		
		public int getGroupCount() 
		
			{
			// TODO Auto-generated method stub
			return pwd.list().length;
			}
		
		@Override
		public int getChildrenCount(int groupPosition) 
		
			{
			int count = 0;
			
			if (pwd.listFiles()[groupPosition].isDirectory())
				
				{
				count = pwd.listFiles()[groupPosition].list().length;
				}
			
			return count;
			}

		
		
		@Override
		public Object getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return null;
		}

		
		
		@Override
		public long getGroupId(int groupPosition) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return 0;
		}

		
		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}

		
		
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) 
		
			{
			LayoutInflater li;
			View layout = null;
			TextView filename;
			
			//if (pwd.listFiles()[groupPosition].isDirectory())
			
				{
				li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				layout = li.inflate(R.layout.group_view, null);
				filename = layout.findViewById(R.id.name);
				((TextView)layout.findViewById(R.id.size)).setText(String.valueOf(pwd.listFiles()[groupPosition].length()/1000) + " KB");
				filename.setText(pwd.listFiles()[groupPosition].getName());
				}
			/*
			else
				
				{
				layout = new TextView(getBaseContext());
				((TextView)layout).setText("No Files");
				((TextView)layout).setHeight(100);
				((TextView)layout).setWidth(parent.getWidth());
				}
			*/
			return layout;
			}
		
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) 
		
			{
			LayoutInflater li;
			View layout = null;
			TextView filename;
			
			//if (pwd.listFiles()[groupPosition].isDirectory())
			
				{
				li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				layout = li.inflate(R.layout.file_view, null);
				filename = layout.findViewById(R.id.name);
				((TextView)layout.findViewById(R.id.size)).setText(String.valueOf(pwd.listFiles()[groupPosition].listFiles()[childPosition].length()/1000) + " KB");
				filename.setText(pwd.listFiles()[groupPosition].list()[childPosition]);
				
				//@SuppressWarnings("deprecation")
				//LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				//params.setMargins(500, 0, 0, 0);
				//layout.setLayoutParams(params);
				}
			/*
			else
				
				{
				layout = new TextView(getBaseContext());
				((TextView)layout).setText("No Files");
				((TextView)layout).setHeight(100);
				((TextView)layout).setWidth(parent.getWidth());
				}
			*/
			return layout;
			}
		

		
		
		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return true;
		}

		
		}

	
	
	// 
	
	private class LocalBroadcastReceiver extends BroadcastReceiver
	
		{

		public void onReceive(Context arg0, Intent arg1) 
		
			{
			
			}
		
		}
	
	}
